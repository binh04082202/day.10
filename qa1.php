<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }
    .main{
        padding-top:20px;
        margin-left: auto;
        margin-right: auto;
        width: 800px;
        height: auto;
        background-color: #3984bc;
    }
    .question_1{

    }
    .question{
        font-size: 20px;
        font-weight: bold;
        text-align: center;
        padding-bottom: 10px;
    }
    .list-answer li {
        font-size: 15px;
        padding-left: 50px;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .cut{
        width: 100%;
        height: 3px;
        background-color: black;
    }
    .btn-next button{
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 16px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        transition-duration: 0.4s;
        cursor: pointer;
    }

    .btn-next button:hover {
        background-color: #4CAF50;
        color: white;
    }
    </style>
</head>
<body>
    <?php
        session_start();
    
        $listQA1 = array("đỏ","cam","đen","hồng");
        $listQA2 = array("H2N-CH2-CONH-CH2-CO-NH-CH2-COOH"," H2N-CH2-CH2-CO-NH-CH2-CH2-COOH","H2N-CH2-CO-NH-CH(CH3)-COOH","H2N-CH2-CH2-CO-CH2-COOH");
        $listQA3 = array("cụ","bác","em","mồm");
        $listQA4 = array("123654","265413","523146","314562");
        $listQA5 = array("-1"," π/6","1","- π/6");

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            
            if(isset($_POST["answer1"])&&isset($_POST["answer2"])&&isset($_POST["answer3"])&&isset($_POST["answer4"])&&isset($_POST["answer5"])){
                $answer1 = $_POST["answer1"];
                $qa1 = "Dòng chữ này có màu gì?";
                $answer2 = $_POST["answer2"];
                $qa2 = "Hợp chất nào sau đây thuộc loại đipeptit ?";
                $answer3 = $_POST["answer3"];
                $qa3 = " A gọi B bằng bác, B gọi C là ông nội , C kêu D là cậu, D kêu E là dì, E kêu F là chú, F gọi Z là con . Hỏi A gọi Z bằng gì ???";
                $answer4 = $_POST["answer4"];
                $qa4 = "Hoàn thành câu tục ngữ sau: làm/ăn/không/mà/có/đòi";
                $answer5 = $_POST["answer5"];
                $qa5 = "Cho hàm số y = 3Sinx - 4sin3x. Giá trị lớn nh ất của han số trên khoảng (-π/2 ; π/2) bằng :";
                setcookie("answer1",$answer1, time() + (86400 * 30), "/");
                setcookie("qa1",$qa1, time() + (86400 * 30), "/");
                setcookie("answer2",$answer2, time() + (86400 * 30), "/");
                setcookie("qa2",$qa2, time() + (86400 * 30), "/");
                setcookie("answer3",$answer3, time() + (86400 * 30), "/");
                setcookie("qa3",$qa3, time() + (86400 * 30), "/");
                setcookie("answer4",$answer4, time() + (86400 * 30), "/");
                setcookie("qa4",$qa4, time() + (86400 * 30), "/");
                setcookie("answer5",$answer5, time() + (86400 * 30), "/");
                setcookie("qa5",$qa5, time() + (86400 * 30), "/");
                header("location: qa2.php");
            }else{
                echo "<div style='color: red;font-size: 20px;font-weight: bold;text-align: center;'>Hãy nhập trả lời tất cả các câu hỏi</div>";
            }

        }
        
    ?>
            <form name="registerForm" method="post" enctype="multipart/form-data" action="">
                <div class="main">
                    <div class="question_1">
                        <div class="question">Dòng chữ này có màu gì?</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA1 as $x){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value= '$x' name='answer1'>"
                                            .$x.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="question_2">
                        <div class="question">Hợp chất nào sau đây thuộc loại đipeptit ?</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA2 as $x){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value='$x' name='answer2'>"
                                            .$x.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="question_3">
                        <div class="question"> A gọi B bằng bác, B gọi C là ông nội , C kêu D là cậu, D kêu E là dì, E kêu F là chú, F gọi Z là con . Hỏi A gọi Z bằng gì ???</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA3 as $x){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value='$x' name='answer3'>"
                                            .$x.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="question_4">
                        <div class="question">Hoàn thành câu tục ngữ sau: làm/ăn/không/mà/có/đòi</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA4 as $x){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value='$x' name='answer4'>"
                                            .$x.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="question_5">
                        <div class="question">Cho hàm số y = 3Sinx - 4sin3x. Giá trị lớn nh ất của han số trên khoảng (-π/2 ; π/2) bằng :</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA5 as $x){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value='$x' name='answer5'>"
                                            .$x.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="btn-next">
                        <button >
                            NEXT
                        </button>
                    </div>  
                </div>
            </form>


</body>
</html>